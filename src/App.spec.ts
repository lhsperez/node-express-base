import 'mocha';
import app from './App';
import { expect, request, use } from 'chai';


describe('Server', () => {
    use(require('chai-http'));
    it(`should respond on / with 'Hello World! in JSON format`, (done) => {
        request(app)
            .get('/')
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.be.equal('Hello World!');
                done();
            })
            .catch((err) => {
                done(err);
            });
    });
});